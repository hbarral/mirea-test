var data = $("form").serialize(); // Serialize form into variable "data"

$.ajax({                          // Initialize ajax function from jQuery library, for http async request.
   type: "GET",                   // Type of the request, can be POST or GET(default)
   dataType: "json",              // The type of data expected back from server.
   url: "index.php",              // Url to which the request is sent.
   data: data                     // The data to send to server.
}).done(function (data) {         // This function is called if the request succeeds.
                                  // code to be executed during the success callback.
}).fail(function (e) {            // This function is called if the request fails.
                                  // code to be executed during the fail callback.
})                                // End of the ajax function.
