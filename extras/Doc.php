<?php

abstract class Doc
{
    protected $name_doc;
    protected $date_upload;

    public function __construct($name_doc, $date_upload){
        $this->date_upload = $date_upload;
        $this->name_doc =  $name_doc;
    }

    public function get_info_upload(){
        return $this->date_upload;
    }

    abstract function upload_doc();
}

