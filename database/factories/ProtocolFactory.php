<?php

use Faker\Generator as Faker;




$factory->define(App\Protocol::class, function (Faker $faker) {
    $protocols = [
        'TCPMUX',
        'Echo',
        'SYSTAT',
        'Daytime',
        'Quote',
        'Send',
        'FTP',
        'SSH',
        'Telnet',
        'SMTP',
        'Time',
        'DNS',
        'RAP',
        'HTTP',
        'Kerberos',
        'Hostname',
        'POP',
        'SFTP',
        'NTP',
        'NETBIOS',
        'IMAP',
        'TCP',
        'SNMP',
        'SMTP'
    ];

    return [
        'num_protocol' => $protocols[rand(0, count($protocols) - 1)],
        'date_protocol' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'system_status' => $faker->numberBetween($min = 0, $max = 1),
        'org_id' => $faker->numberBetween($min = 1, $max = 30)
    ];
});
