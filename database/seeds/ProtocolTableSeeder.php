<?php

use Illuminate\Database\Seeder;

class ProtocolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Protocol::class, 90)->create();
    }
}
