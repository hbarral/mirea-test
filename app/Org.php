<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Org extends Model
{
    public function protocols()
    {
       return $this->hasMany(Protocol::class);
    }

    public function protocols_ids()
    {
        $data = $this->hasMany(Protocol::class);
        $onlyID = [];


        foreach ($data as $da)

        $onlyID[] = $da->id;

        return $onlyID;
    }


}
