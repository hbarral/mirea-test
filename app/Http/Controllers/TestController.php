<?php

namespace App\Http\Controllers;

use App\Org;
use Illuminate\Http\Request;
use DB;
use Highlight\Highlighter;
use File;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function test1(Request $request)
    {
        $limit = $request->input('limit') ? : 10;
        $offset = $request->input('offset') ? : 0;
        $orderColumn = $request->input('sort') ? : 'id';
        $orderDirection = $request->input('asc') ? : 'asc';
        $total = Org::count();
        $result = [];


        DB::enableQueryLog();
        $orgs = Org::with('protocols')
            ->orderBy($orderColumn, $orderDirection)
            ->limit($limit)
            ->offset($offset)
            ->get();

        $laQuery = DB::getQueryLog();
        DB::disableQueryLog();


        foreach ($orgs as $org)
        {
            $protocols_ids = [];
            $protocolsNames = [];
            foreach($org->protocols as $protocol)
            {
                $protocols_ids[] = $protocol->id;
                $protocolsNames[] = $protocol->num_protocol;
            }

            unset($org['protocols']);
            unset($org['created_at']);
            unset($org['updated_at']);

            $result[] =  array_merge(
                $org->toArray(),
                ['protocols_ids' => $protocols_ids],
                ['protocols_names' => $protocolsNames]
            );
        }


        if($request->ajax()){
            return response()->json(['rows' => $result, 'total' => $total, 'queryDB' => [$laQuery[0]['query'], $laQuery[1]['query']]], 200);

        }

        return view('test1');
    }

    public function test2()
    {

        $base =  base_path() . '/extras/';
        $first = $base . 'index.php';
        $second = $base . 'Doc.php';
        $third = $base . 'Letter.php';

        $content[] = File::get($first);
        $content[] = File::get($second);
        $content[] = File::get($third);


        $highlighter = new Highlighter();
        $language = "php";


        $markupHighlightedCodeObject = [];
        foreach ($content as $code)
        {
            $markupHighlightedCodeObject[] = $highlighter->highlight($language, $code);
        }

        return view("test2", [
            "first" => $markupHighlightedCodeObject[0],
            "second" => $markupHighlightedCodeObject[1],
            "third" => $markupHighlightedCodeObject[2]
        ]);
    }
    public function test3()
    {

        $base =  base_path() . '/extras/';
        $filename = $base . 'ajaxFunction.js';

        $content = File::get($filename);

        $highlighter = new Highlighter();
        $language = "javascript";
        $markupHighlightedCodeObject = $highlighter->highlight($language, $content);

        return view("test3", [
            "code" => $markupHighlightedCodeObject,
        ]);
    }
}
