<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Protocol extends Model
{
    public function org()
    {
        return $this->belongsTo(Org::class);
    }
}
