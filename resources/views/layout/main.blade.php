
{{-- <!doctype html> --}}
{{-- <html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> --}}
{{--     <head> --}}
{{--         <meta charset="utf-8"> --}}
{{--         <meta name="viewport" content="width=device-width, initial-scale=1"> --}}



{{--     </head> --}}
{{--     <body> --}}
{{--     </body> --}}
{{-- </html> --}}

<!DOCTYPE html>
<html lang="us">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Mirea - @yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/app.css">
    @stack('styles')
  </head>
  <body>
      <nav class="navbar navbar-expand-md navbar-dark bg-primary">
          <a href="/" class="navbar-brand">Mirea</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mirea">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="navbar-collapse collapse justify-content-stretch" id="navbar-mirea">
              <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                      <a class="nav-link" target="_blank" href="https://gitlab.com/hbarral/mirea-test"><i class="fa fa-gitlab mr-1"></i>
                      </a>
                  </li>

                  <li class="nav-item">
                      <a class="nav-link" href="/test1">Test 1</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="/test2">Test 2</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="/test3">Test 3</a>
                  </li>
              </ul>
          </div>
      </nav>
    <div class="container">
      <div class="content">
        @yield('content')
      </div>
    </div>



    <script src="js/app.js"></script>
    @stack('scripts')
  </body>
</html>
