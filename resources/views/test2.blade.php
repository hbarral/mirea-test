@extends('layout.main')

@section('title', 'Test 2')

@section('content')
  <h1 class="h1 text-center">Test 2</h1>

  <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
      <li class="nav-item">
          <a class="nav-link active" id="pills-index-tab" data-toggle="pill" href="#pills-index" role="tab" aria-controls="pills-index" aria-selected="true">index.php</a>
      </li>
      <li class="nav-item">
          <a class="nav-link" id="pills-doc-tab" data-toggle="pill" href="#pills-doc" role="tab" aria-controls="pills-doc" aria-selected="false">Doc.php</a>
      </li>
      <li class="nav-item">
          <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Letter.php</a>
      </li>
  </ul>
  <div class="tab-content" id="pills-tabContent">
      <div class="tab-pane fade show active" id="pills-index" role="tabpanel" aria-labelledby="pills-index-tab">
          <pre>
          <code class="hljs {{ $first->language }}">{!! $first->value !!}</code>
          </pre>
      </div>
      <div class="tab-pane fade" id="pills-doc" role="tabpanel" aria-labelledby="pills-doc-tab">
            <pre>
          <code class="hljs {{ $second->language }}">{!! $second->value !!}</code>
          </pre>
      </div>
      <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
            <pre>
          <code class="hljs {{ $third->language }}">{!! $third->value !!}</code>
          </pre>
      </div>
  </div>
@endsection
