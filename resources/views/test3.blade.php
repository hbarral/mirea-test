@extends('layout.main')

@section('title', 'Test 3')

@section('content')
    <h1 class="h1 text-center">Test 3</h1>

    <pre>
          <code class="hljs {{ $code->language }}">{!! $code->value !!}</code>
    </pre>
@endsection
