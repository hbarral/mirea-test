@extends('layout.main')

@section('title', 'Test 1')

@section('content')
  <h1 class="h1 text-center">Test 1</h1>

  <div id="app">
      <div class="row">
          <div class="col-md-10 offset-1">
              <hb-datatable></hb-datatable>
          </div>
      </div>
  </div>

@endsection
